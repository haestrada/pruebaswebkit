//
//  WebViewController.swift
//  pruebasEI
//
//  Created by Hugo Estrada on 12/10/17.
//  Copyright © 2017 Nada. All rights reserved.
//

import UIKit

class WebViewController: UIViewController,UIWebViewDelegate, UITableViewDelegate, UITableViewDataSource {
    // Se usa UIWebView aunque esté deprecado, ya que es compatible con ios9+
    @IBOutlet weak var webView: UIWebView!
    
    //Constraint del table view
    @IBOutlet weak var tableHeightConstraint: NSLayoutConstraint!
    // Constraint de todo el contenedor
    @IBOutlet weak var containerHeightConstraint: NSLayoutConstraint!
    // Constraint del WebView
    @IBOutlet weak var webHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var tableView: UITableView!
    var startHeight:CGFloat = 0
    var wvHeight:CGFloat = 0
    var tvHeight:CGFloat = 0
    
    let totRows = 20 // Simular total de filas de los comentarios
    
    override func viewDidLoad() {
        super.viewDidLoad()
        startHeight=containerHeightConstraint.constant
       webView.delegate=self
        
    webView.scrollView.isScrollEnabled = false
        let contenido = "<div class=\"post-text\" itemprop=\"text\"><p>If the images are hosted on the Stack Exchange imgur.com account (which most are), you can add a <a href=\"https://api.imgur.com/models/image#thumbs\" rel=\"nofollow noreferrer\"><code>h</code>, <code>l</code>, <code>m</code>, <code>t</code>, <code>b</code> or <code>s</code></a> to the filename in the URL (before the extension) to get resized versions.</p><p>For that post, for example, you can use<br> <code>https://i.stack.imgur.com/a9LCgl.jpg</code> instead of<br> <code>https://i.stack.imgur.com/a9LCg.jpg</code> to get a more manageable size screenshot.</p><p>If needed, you can always link to the full-size screenshot too; transform:</p><pre><code>[![enter image description here][1]][1]  [1]: https://i.stack.imgur.com/a9LCg.jpg</code></pre><p>to</p><pre><code>[![enter image description here][1]][2]  [1]: https://i.stack.imgur.com/a9LCgl.jpg  [2]: https://i.stack.imgur.com/a9LCg.jpg</code></pre><p>and it'll link the reduced-size version to the full-size image.</p><p>I've gone ahead and used the medium (<code>m</code>) version for that post to reduce the size of the retina-resolution iPhone screenshots, linking each to the full-resolution version.</p><p>For completeness sake, the letters stand for:</p><pre><code>s: small   square     90×90   (forced)b: big     square     160×160 (forced)t: small   thumbnail  160×160m: medium  thumbnail  320×320l: large   thumbnail  640×640h: huge    thumbnail  1024×1024</code></pre><p>The square options will scale the image (up as well as down) and crop to fit the size, thumbnails are only ever scaled down (smaller images are not scaled up) to fit with the square maximum dimensions.</p><p><strong>NOTE:</strong> Resizing only the preserves the first frame of an animated GIF image. If you need to resize an animation, use HTML markup instead:</p><pre class=\"lang-html prettyprint prettyprinted\" style=\"\"><code><span class=\"tag\">&lt;img</span><span class=\"pln\"> </span><span class=\"atn\">src</span><span class=\"pun\">=</span><span class=\"atv\">\"https://i.stack.imgur.com/a9LCg.jpg\"</span><span class=\"pln\"> </span><span class=\"atn\">width</span><span class=\"pun\">=</span><span class=\"atv\">\"400\"</span><span class=\"pln\"> </span><span class=\"tag\">/&gt;</span></code></pre><p>The <code>width</code> attribute tells the browser to constrain the size, the height is scaled along automatically. </p>    </div>"
        webView.loadHTMLString(contenido, baseURL: nil)
        
        // Alto del tableview
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // Simulación
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return totRows
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell")
        let lbl = cell?.viewWithTag(1) as! UILabel
        
        lbl.text = "Fila No. \(indexPath.row)"
        return cell!
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        print("loading \(indexPath.row)")
        if indexPath.row == tableView.indexPathsForVisibleRows?.last?.row {
           tableHeightConstraint.constant=tableView.contentSize.height
            tvHeight = tableView.contentSize.height
            calcTotalHeight()
            
        }
    }
    func calcTotalHeight(){
        print("start: \(startHeight), WebView: \(wvHeight) tableView: \(tvHeight)")
        containerHeightConstraint.constant = startHeight + wvHeight + tvHeight
    }
    func webViewDidFinishLoad(_ webView: UIWebView) {
        // Calculando el HEIGHT del web view
        let str = webView.stringByEvaluatingJavaScript(from: "(document.height !== undefined) ? document.height : document.body.offsetHeight;")
        print("calculado H:",str!)
        self.wvHeight = CGFloat((str! as NSString).floatValue)+10
        webHeightConstraint.constant = wvHeight
        calcTotalHeight()
        
    }
}
