//
//  ViewController.swift
//  pruebasEI
//
//  Created by Hugo Estrada on 12/10/17.
//  Copyright © 2017 Nada. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    override func viewWillAppear(_ animated: Bool) {
        let bar = self.navigationController?.navigationBar as! AGNavigationBarShape
        bar.color = UIColor.gray
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

